<?php

namespace Clear\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UsuarioCriado extends Notification
{
    use Queueable;

    public $token;
    public $login;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $login)
    {
      $this->token = $token;
      $this->login = $login;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
      $link = url(config('app.url').route('criar-senha', [
        'token' => $this->token,
        'login' => $this->login,
      ], false));

      return (new MailMessage)
                  ->subject('Concurso CLEAR - Usuário criado')
                  ->greeting('Usuário criado')
                  ->line('Seu usuário para o concurso CLEAR de Casos Clínicos foi criado com sucesso.')
                  ->action('Cadastrar Senha', $link)
                  ->line('Acesse o sistema para poder cadastrar uma senha e efetuar o login.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
