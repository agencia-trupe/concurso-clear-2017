<?php

namespace Clear\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificarFarmacovigilancia extends Mailable
{
    use Queueable, SerializesModels;

    protected $caso;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($caso)
    {
      $this->caso = $caso;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('vendor.notifications.farmaco', [
                      'titulo' => 'Caso clínico: '.$this->caso->codigo.' (categoria: '.$this->caso->categoria.')',
                      'coordenador' => 'Enviado por: '.$this->caso->coordenador->nome.' ('.$this->caso->coordenador->email.')'
                    ])
                    ->subject('Concurso CLEAR - Novo caso clínico')
                    ->attach(env('SITE_ARQUIVOS_DIR').$this->caso->arquivo);
    }
}
