<?php

namespace Clear\Http\Controllers;

use Auth;
use Clear\Models\User;
use Clear\Models\Avaliacao;
use Carbon\Carbon as Carbon;
use Illuminate\Http\Request;

class AvaliacoesController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(){
    return view('avaliacoes.index', [
      'avaliacoesCat1' => Auth::user()->avaliacoes()->novas(1),
      'avaliacoesCat2' => Auth::user()->avaliacoes()->novas(2),
      'historicoCat1'  => Auth::user()->avaliacoes()->historico(1),
      'historicoCat2'  => Auth::user()->avaliacoes()->historico(2)
    ]);
  }

  public function buscar(){
    return [
      'novas' => [
        'categoria1' => Auth::user()->avaliacoes()->with('caso')->novas(1),
        'categoria2' => Auth::user()->avaliacoes()->with('caso')->novas(2),
      ],
      'historico' => [
        'categoria1' => Auth::user()->avaliacoes()->with('caso')->historico(1),
        'categoria2' => Auth::user()->avaliacoes()->with('caso')->historico(2)
      ]
    ];
  }

  public function enviarNotas(Request $request)
  {
    $this->validate($request,[
      'notas.criterio_1' =>  'required|numeric|min:1|max:10',
      'notas.criterio_2' =>  'required|numeric|min:1|max:10',
      'notas.criterio_3' =>  'required|numeric|min:1|max:10',
      'notas.criterio_4' =>  'required|numeric|min:1|max:10',
      //'notas.criterio_5' =>  'required|notaValida',
      //'notas.caso' => 'required|exists:casos,id',
      'notas.avaliacao' => 'required|exists:avaliacoes,id|avaliacaoPodeSerAtualizada'
    ]);

    try {

      $avaliacao = Avaliacao::findOrFail($request->notas['avaliacao']);

      $avaliacao->criterio_1 = $request->notas['criterio_1'];
      $avaliacao->criterio_2 = $request->notas['criterio_2'];
      $avaliacao->criterio_3 = $request->notas['criterio_3'];
      $avaliacao->criterio_4 = $request->notas['criterio_4'];
      $avaliacao->avaliado_em = Carbon::now();
      $avaliacao->media = media_ponderada($avaliacao->criterio_1,$avaliacao->criterio_2,$avaliacao->criterio_3,$avaliacao->criterio_4);

      $avaliacao->save();

    } catch (Exception $e) {

      $erro = logar_erro($e, Auth::user()->id);
      return back()->withErrors(array('Não foi possível enviar a Avaliação. (Erro: '.$erro.')'));

    }

  }

}
