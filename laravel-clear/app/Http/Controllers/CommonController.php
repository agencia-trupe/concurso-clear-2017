<?php

namespace Clear\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommonController extends Controller
{

  public function __construct()
  {
    $this->middleware(['auth', 'auth.ativos']);
  }

  public function home(){
    return view('common.home');
  }

  public function regulamento(){
    return view('common.regulamento');
  }

  public function cronograma(){
    return view('common.cronograma');
  }

  public function avaliadores(){
    $avaliadores = \Clear\Models\User::avaliadores()->orderBy('nome', 'asc')->get();
    return view('common.avaliadores', compact('avaliadores'));
  }

}
