@extends('template.index')

@section('conteudo')
<div id="app-vue">
  <div class="conteudo conteudo-submeter-caso com-recuoo">
    <div class="centralizar reduzido">

      @if(session('sucesso'))
        <p class="alerta alerta-sucesso auto-close">
          {{session('sucesso')}}
        </p>
      @endif

      <p class="small">
        Os trabalhos deverão ser desenvolvidos no arquivo modelo conforme
        estrutura indicada e ser submetidos exclusivamente pelo website
        <a href="www.concursoclear.com.br">www.concursoclear.com.br</a> até as
        23h59 min do dia 07 de agosto de 2017.
        Não serão aceitos relatos de caso que não utilizem o arquivo modelo ou
        que tenham sido submetidos após o horário e data limites.
      </p>

      <a href="download-formulario" class="download-link" title="Download do formulário">
        <span>
          <img src="images/icone_download.png" alt="Download">
        </span>
        DOWNLOAD DO FORMULÁRIO
      </a>

      @if(Auth::user()->isCoordenador)

        <p>
          <strong>Envie aqui seu caso clínico.</strong>
          <br>
          IMPORTANTE: faça o download do formulário, preencha e salve em PDF, que
          deve ser o formato enviado.
        </p>

        <div class="botoes-envio">

          <div class="box">
            @if($casoCat1->isEnviado)

              <div class="casoEnviado">
                <p>
                  Arquivo do caso <strong>CATEGORIA 1</strong><br> enviado em: {{$casoCat1->enviado_em->format('d/m/Y H:i\h')}}.
                </p>
              </div>

            @else

              @if($casoCat1->podeSerEnviado)
                <a href="#" title="Enviar caso clínico" id="mostrarFormCat1" @click.prevent="mostrarForm(1)">
                  <h3>CATEGORIA 1</h3>
                  <p>
                    Caso clínico de paciente bio-naive (sem exposição prévia à
                    terapia imunobiológica) em uso de secuquinumabe
                  </p>
                  <div class="botao">
                    <span>
                      <img src="images/icone-enviar.png" alt="Enviar arquivo">
                    </span>
                    <span>
                      ENVIAR CASO CLÍNICO
                    </span>
                  </div>
                </a>

              @else

                <div class="prazoExpirado">
                  <p>
                    O prazo máximo para o envio de casos expirou em <strong>{{env('SITE_DATA_MAX_ENVIO')}}</strong>.
                  </p>
                </div>

              @endif

            @endif
          </div>

          <div class="box">
            @if($casoCat2->isEnviado)

              <div class="casoEnviado">
                <p>
                  Arquivo do caso <strong>CATEGORIA 2</strong><br> enviado em: {{$casoCat2->enviado_em->format('d/m/Y H:i\h')}}.
                </p>
              </div>

            @else

              @if($casoCat2->podeSerEnviado)
                <a href="#" title="Enviar caso clínico" id="mostrarFormCat2" @click.prevent="mostrarForm(2)">
                  <h3>CATEGORIA 2</h3>
                  <p>
                    Caso clínico de paciente com exposição prévia à terapia
                    imunobiológica em uso de secuquinumabe
                  </p>
                  <div class="botao">
                    <span>
                      <img src="images/icone-enviar.png" alt="Enviar arquivo">
                    </span>
                    ENVIAR CASO CLÍNICO
                  </div>
                </a>

              @else

                <div class="prazoExpirado">
                  <p>
                    O prazo máximo para o envio de casos expirou em <strong>{{env('SITE_DATA_MAX_ENVIO')}}</strong>.
                  </p>
                </div>

              @endif

            @endif
          </div>

        </div>

      @endif

    </div>
  </div>

  <transition name="fade">
    <div class="modal" v-show="form.mostrar" v-cloak>
      <div class="backdrop" @click.stop="fecharModal"></div>
      <div class="modal-conteudo">
        <form class="formCaso" action="{{route('submeter-caso')}}" method="post" enctype="multipart/form-data">
          {!! csrf_field() !!}

          <h2>ENVIO DE CASO CLÍNICO - <span>CATEGORIA @{{form.categoria}}</span></h2>

          @if($errors->any())
            <p class="alerta alerta-erro" id="retorno-submissao">
              {{$errors->first()}}
            </p>
          @endif

          <input type="hidden" name="_categoria" :value="form.categoria">
          <input type="hidden" name="_categoria_old" value="{{old('_categoria')}}">

          <div class="input-arquivo">
            <label for="inputArquivo">
              <span class="placeholder">
                [ENVIAR ARQUIVO - <strong>FORMATO PDF</strong>]
              </span>
              <span class="botao">Selecionar arquivo</span>
            </label>
            <input type="file" name="arquivo" required id="inputArquivo">
          </div>
          <input type="text" name="autor_principal" placeholder="Autor Principal" value="{{old('autor_principal')}}" required>
          <p>
            Garantir que o autor principal do relato de caso seja o médico que
             acompanha o paciente. O autor principal deve ser dermatologista.
             Em caso de mais de um médico acompanhando o paciente, um deles deve
             ser apontado como autor principal.
          </p>
          <input type="text" name="co_autor_1" placeholder="Co-autor 1 (se houver)" value="{{old('co_autor_1')}}">
          <input type="text" name="co_autor_2" placeholder="Co-autor 2 (se houver)" value="{{old('co_autor_2')}}">
          <input type="text" name="co_autor_3" placeholder="Co-autor 3 (se houver)" value="{{old('co_autor_3')}}">
          <input type="text" name="co_autor_4" placeholder="Co-autor 4 (se houver)" value="{{old('co_autor_4')}}">
          <input type="text" name="co_autor_5" placeholder="Co-autor 5 (se houver)" value="{{old('co_autor_5')}}">
          <input type="submit" value="ENVIAR" @click.once="">
        </form>
      </div>
    </div>
  </transition>

</div>
@endsection
