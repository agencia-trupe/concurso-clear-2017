@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-cronograma com-recuoo">
    <div class="centralizar">

      <div class="lista-cronograma">

        <div class="linha">
          <div class="data">
            <span>
              25 de fevereiro:
            </span>
          </div>
          <div class="texto">
            <p>
              prazo para indicações dos médicos coordenadores pela área comercial Novartis
            </p>
          </div>
        </div>

        <div class="linha">
          <div class="data">
            <span>
              Março:
            </span>
          </div>
          <div class="texto">
            <p>
              prazo para validação e convite aos médicos coordenadores pela área médica Novartis
            </p>
          </div>
        </div>

        <div class="linha">
          <div class="data">
            <span>
              18 de março:
            </span>
          </div>
          <div class="texto">
            <p>
              reunião presencial com os coordenadores e membros da comissão julgadora para apresentação do programa
            </p>
          </div>
        </div>

        <div class="linha">
          <div class="data">
            <span>
              De 20 de março a 07 de agosto:
            </span>
          </div>
          <div class="texto">
            <p>
              reuniões dos grupos de trabalho para discussão dos casos clínicos.
            </p>
          </div>
        </div>

        <div class="linha">
          <div class="data">
            <span>
              Até 07 de agosto:
            </span>
          </div>
          <div class="texto">
            <p>
              submissão dos casos clínicos para avaliação do comitê avaliador
            </p>
          </div>
        </div>

        <div class="linha">
          <div class="data">
            <span>
              De 08 de Agosto a 15 de setembro:
            </span>
          </div>
          <div class="texto">
            <p>
              avaliação dos relatos de casos clínicos pelo comitê avaliador
            </p>
          </div>
        </div>

        <div class="linha">
          <div class="data">
            <span>
              30 de setembro:
            </span>
          </div>
          <div class="texto">
            <p>
              fórum de encerramento com divulgação dos vencedores e cerimônia de premiação.
            </p>
          </div>
        </div>

        <div class="linha">
          <div class="data">
            <span>
              2018:
            </span>
          </div>
          <div class="texto">
            <p>
              premiação - publicação de material promocional com caso clínico comentado para cada grupo vencedor e apoio para Congresso Brasileiro de Dermatologia 2018 para todos os integrantes dos grupos vencedores.
            </p>
          </div>
        </div>
      </div>

    </div>
  </div>

@endsection
