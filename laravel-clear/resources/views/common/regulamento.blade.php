@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-regulamento com-recuoo">
    <div class="centralizar">

      <h2>REGULAMENTO - CONCURSO CLEAR DE CASOS CLÍNICOS 2017</h2>

      <h3>I – DO PROGRAMA</h3>

      <p>
        Promovido pela Novartis Biociências S/A, o Concurso CLEAR de Relatos de
        Casos Clínicos 2017 envolverá o envio de relatos de casos clínicos de
        psoríase e artrite psoriásica de pacientes em uso de secuquinumabe por
        grupos de trabalho organizados pela Novartis envolvendo médicos do Brasil.
        Website exclusivo: www.concursoclear.com.br.
      </p>

      <h3>I – DOS OBJETIVOS</h3>

      <p>
        Difundir o conhecimento técnico e científico sobre o tratamento da
        psoríase e artrite psoriásica com o uso de secuquinumabe entre médicos.
        Permitir o intercâmbio de boas práticas médicas dirigidas à área de
        imunologia entre médicos dermatologistas e especialidades afins do Brasil.
      </p>

      <h3>III – DA ELEGIBILIDADE DOS PARTICIPANTES</h3>

      <p>
        Os participantes do concurso deverão atender aos seguintes critérios:
        <ul>
          <li>
            Ser médico e atuar no Brasil
          </li>
          <li>
            Estar no painel de visitação da área comercial Novartis da linha
            Cosentyx Dermatologia
          </li>
        </ul>
      </p>

      <p>
        Os participantes do concurso serão selecionados e convidados pela Novartis
         com base nos critérios acima após validação pela área médica. Serão
         formados grupos de trabalho de 4 a 6 médicos participantes, que terão
         encontros organizados pela área comercial Novartis para discussão de
         casos clínicos. O número de grupos será de 2 por consultor técnico Novartis
          (com exceção permitida no máximo para 3 grupos), sendo 12 consultores
          técnicos da linha promocional Cosentyx Dermatologia.
      </p>

      <p>
        Os participantes assinarão termo de participação no primeiro encontro do
         grupo de trabalho, comprovando o cumprimento dos critérios de
         participação acima.
      </p>

      <p>
        Cada grupo de trabalho terá 1 coordenador (já considerado dentro do número
        de 4 a 6 médicos de cada grupo de trabalho). O coordenador será indicado
         pela área comercial Novartis e convidado oficialmente pela área médica
         Novartis e sujeitos à validação da Área Médica da Novartis, conforme
         processo descrito a seguir:
      </p>

      <p>
        Os coordenadores deverão enviar seus currículos e preencher um formulário
         padrão da Área Médica Novartis, com consequente validação de sua
         participação pela Medicina baseada na constatação de alguns dos seguintes
          critérios (adicionais aos válidos para todos os participantes):
      </p>

      <ul>
        <li>Palestrante em eventos de sociedade médica nos últimos 5 anos</li>
        <li>Posição em departamento científico de sociedade médica / científica</li>
        <li>Mínimo de 5 anos de experiência em dermatologia </li>
        <li>Publicação em revistas indexadas nos últimos 5 anos (autor ou coautor)</li>
        <li>Vínculo com instituição de saúde (hospital, universidade etc.), exceto
          clínica e consultórios privados</li>
        <li>Membro de centro de referência de psoríase e/ou artrite psoriásica </li>
        <li>Título de especialista em dermatologia</li>
        <li>Participante de pesquisa clínica em dermatologia nos últimos 5 anos</li>
        <li>Membro de Advisory Board nos últimos 2 anos</li>
        <li>Posição de liderança em hospitais-escola ou Universidades nos últimos 5 anos</li>
        <li>Múltipla especialização na área terapêutica</li>
        <li>Palestrante em eventos internacionais nos últimos 5 anos</li>
      </ul>

      <p>
        O coordenador de cada grupo terá as seguintes atribuições:
      </p>

      <ul>
        <li>
          Liderar reuniões com o seu grupo de trabalho e ser responsável pelo
          engajamento dos membros do grupo
        </li>
        <li>
          Recolher o formulário de participação no Concurso de todos os membros
          do grupo e entregar para o participante Novartis na primeira reunião
          do grupo de trabalho
        </li>
        <li>
          Apresentar dados de secuquinumabe para o tratamento de psoríase e
          artrite psoriásica, utilizando o slide kit padrão enviado pela área
          Médica Novartis.
        </li>
        <li>
          Esclarecer dúvidas dos membros do grupo
        </li>
        <li>
          Facilitar as discussões do grupo em relação aos casos clínicos
        </li>
        <li>
          Assegurar que os casos discutidos não envolvem indicações não aprovadas
           em bula
        </li>
        <li>
          Assegurar que os eventos adversos que possam ser identificados durante
           as apresentações dos casos clínicos serão relatados à Farmacovigilância
           da Novartis
        </li>
        <li>
          Garantir que os casos que serão submetidos ao Concurso possuem consentimento
           do paciente, por escrito, para divulgação do relato e das fotos. Informações
            de pacientes serão anonimizadas e as fotos não permitirão a identificação
            do paciente.
        </li>
        <li>
          Garantir que o autor principal do relato de caso seja o médico que acompanha
           o paciente. O autor principal deve ser dermatologista. Em caso de mais
           de um médico acompanhando o paciente, um deles deve ser apontado como autor
            principal.
        </li>
        <li>
          Realizar a submissão dos relatos de casos clínicos escolhidos pelo grupo
           para o Concurso CLEAR de relatos de casos clínicos, sendo obrigatório
            o envio de, no mínimo, 01 caso clínico do seu grupo.
        </li>
      </ul>

      <p>
        O coordenador receberá FEE dentro da política Novartis para sua prestação
        de serviço dentro das atribuições relacionadas acima.
      </p>

      <p>
        O Comitê Avaliador será formado por até 6 participantes, médicos
        dermatologistas do Brasil, selecionados pela área médica da Novartis, os
         quais atuarão com total independência em relação à Novartis. Cada caso
         será avaliado por 3 membros do Comitê, selecionados aleatoriamente.
      </p>

      <ul>
        <li>
          Os critérios de avaliação serão (Pontuação total possível por
          avaliador: 100 pontos):
          <ul>
            <li>1. Título 1-10 pontos (peso 1)</li>
            <li>2. Relato do caso clínico 1-10 pontos (peso 4)</li>
            <li>3. Revisão da literatura 1-10 pontos (peso 2)</li>
            <li>4. Discussão e conclusão 1-10 pontos (peso 3)</li>
            <li>5. Referências – sem pontuação</li>
          </ul>
        </li>
        <li>Cada caso clínico pode somar até 300 pontos.</li>
        <li>
          Se existir empate após a conciliação das notas dos três avaliadores,
          os critérios de desempate seguirão a seguinte sequência:
          <ul>
            <li>Maior pontuação obtida em Relato de caso clínico</li>
            <li>Maior pontuação obtida em Discussão e conclusão</li>
            <li>Maior pontuação em Revisão da literatura</li>
            <li>Maior pontuação obtida em Título</li>
          </ul>
        </li>
      </ul>

      <p>
        Os trabalhos não serão avaliados pelo(s) membro(s) do Comitê Avaliador
        da mesma cidade / município do autor principal do caso do grupo de trabalho,
         visando garantir imparcialidade na avaliação.
      </p>

      <p>
        Os casos serão avaliados de forma sigilosa – o avaliador não terá
        informação do nome dos médicos, Hospital ou cidade do relato de caso a
        ser avaliado.
      </p>

      <h3>V – DO TEMA</h3>

      <p>
        O concurso será destinado aos grupos de médicos pré-selecionados conforme
         item III deste regulamento, que participarem das reuniões de discussão
         de casos clínicos organizadas pela Novartis, apresentando casos clínicos
          em 2 categorias distintas:
      </p>

      <ul>
        <li>Categoria 1: Caso clínico de paciente bio-naive (sem exposição prévia
           à terapia imunobiológica) em uso de secuquinumabe</li>
        <li>Categoria 2: Caso clínico de paciente com exposição prévia à terapia
          imunobiológica em uso de secuquinumabe </li>
      </ul>

      <p>
        Só serão aceitos casos clínicos em que o paciente estiver em uso de
        secuquinumabe no momento da submissão. Para ambas as categorias serão
        considerados pacientes que possuem psoríase, associada ou não à artrite
        psoriásica.
      </p>

      <h3>VI – DA PREPARAÇÃO E SUBMISSÃO DOS CASOS CLÍNICOS</h3>

      <p>
        A estrutura do caso clínico deverá respeitar os campos indicados no
        arquivo modelo enviado via e-mail aos coordenadores que forem participar
         das reuniões de discussão de casos clínicos organizadas pela Novartis e
          também disponível no website do Concurso.
      </p>

      <p>A estrutura para o relato de caso clínico compreende:</p>

      <ul>
        <li>1. Título </li>
        <li>2. Relato do caso clínico </li>
        <li>3. Revisão da literatura </li>
        <li>4. Discussão e conclusão</li>
        <li>5. Referências </li>
      </ul>

      <p>
        Os relatos de casos clínicos podem ter até 6 autores, sendo um indicado
        como autor principal, que deve ser o médico que acompanha o paciente em
        questão.
      </p>

      <p>
        Deve-se preservar a identidade do médico e do Serviço/Hospital onde o
        paciente do caso clínico foi atendido, não havendo nenhuma citação ou
         referência ao nome, local e/ou cidade no título e corpo do texto do relato
          de caso.
      </p>

      <p>
        É essencial preservar a identidade dos pacientes. Não usar o nome ou
        iniciais do paciente, omitir detalhes que possam identificar as pessoas,
        caso não sejam essenciais para o relato do caso.
      </p>

      <p>
        Necessário ter o consentimento, por escrito, do paciente para que seus
        dados e imagens sejam utilizados no relato de caso clínico submetido ao
        Concurso CLEAR. Informações de pacientes serão anonimizadas e as fotos não
         permitirão a identificação do paciente.
      </p>

      <p>
        Não serão aceitos relatos de casos clínicos que considerem o uso de
        secuquinumabe para indicações e posologia diferentes das de bula no país.
      </p>

      <p>
        É de responsabilidade dos autores a exatidão das referências bibliográficas
         utilizadas no trabalho.
      </p>

      <p>
        Os trabalhos deverão ser desenvolvidos no arquivo modelo conforme estrutura
        indicada e ser submetidos exclusivamente pelo website www.concursoclear.com.br
        até as 23h59 min do dia 07 de agosto de 2017. Não serão aceitos relatos de
        caso que não utilizem o arquivo modelo ou que tenham sido submetidos após o
        horário e data limites.
      </p>

      <p>
        Poderá ser submetido somente 01 relato de caso de cada categoria por
        grupo de trabalho.
      </p>

      <p>
        O caso clínico deve ser enviado dentro da data limite: 07 de agosto de 2017.
      </p>

      <h3>VII – DA DINÂMICA DO CONCURSO E DIVULGAÇÃO DO RESULTADO</h3>

      <p>
        De acordo com os critérios definidos na sessão III, serão formados grupos
         de discussão de casos clínicos a partir de convite da área comercial
          Novartis. Cada grupo terá entre 4 a 6 membros, sendo um coordenador,
          que ficará responsável pelo alinhamento e coordenação dos trabalhos de
           seu grupo.
      </p>

      <p>
        Entre março e agosto de 2017, com suporte e organização da Novartis
        (locação de sala para evento, equipamentos, refeição, logística de
        participantes e FEE do coordenador), serão realizadas reuniões dos grupos
        de discussão para o debate sobre casos clínicos. Ao final das reuniões,
        cada grupo selecionará um relato de caso clínico de cada categoria que
        deverá ser submetido ao concurso CLEAR.
      </p>

      <p>
        Representantes da Novartis estarão presentes nas reuniões, porém não terão
         qualquer ingerência ou interferência nas discussões dos grupos de trabalho,
         a não ser sob solicitação e caso possam colaborar com o grupo, dentro das
         limitações de informações dos presentes, considerando o cargo de cada
         representante Novartis presente na reunião.
      </p>

      <p>
        A área médica dará o suporte para o conteúdo de secuquinumabe a ser
        apresentado. As aulas a serem apresentadas pelos coordenadores nos grupos
        de trabalho deverão utilizar o slide kit padrão enviado pela área Médica
         Novartis.
      </p>

      <p>
        Os casos serão avaliados de forma sigilosa através de ferramenta de votação,
        seguindo os critérios mencionados na sessão IV. Serão escolhidos 3 grupos
        vencedores do concurso, sendo eles:
      </p>

      <ul>
        <li>Vencedor 1: Caso com maior pontuação geral em ambas as categorias</li>
        <li>Vencedor 2: Caso com maior pontuação na categoria 1** </li>
        <li>Vencedor 3: Caso com maior pontuação na categoria 2** </li>
      </ul>

      <p>
        **Ou segunda maior pontuação, considerando se o Vencedor 1 for de sua categoria.
      </p>

      <p>
        Vencedores serão divulgados durante Fórum Novartis a ser realizado em 30
        de setembro. Para este evento, todos os grupos de trabalho (coordenadores
        + participantes), além dos membros do comitê avaliador, serão convidados
         e a Novartis oferecerá apoio logístico com hospedagem e transporte.
      </p>

      <p>
        Os relatos de caso vencedores poderão ser publicados em material promocional
         Novartis. A assinatura do termo de participação formaliza o consentimento
          do autor.
      </p>

      <p>
        Além disso, os vencedores poderão ser convidados pela Novartis para
        apresentar seus relatos de casos clínicos em eventos Novartis.
      </p>

      <h3>VIII – DA PREMIAÇÃO</h3>

      <p>
        Os grupos vencedores (coordenador e demais membros do grupo) receberão o
         apoio educacional para a participação no Congresso Brasileiro de Dermatologia
         2018, além de terem o caso clínico comentado publicado em material
         promocional desenvolvido pela Novartis.
      </p>

      <p>
        O apoio educacional acima mencionado (exclusivamente para os membros e
        coordenadores dos grupos vencedores) é de caráter pessoal e intransferível
        para cada membro do grupo e não poderá ser convertido em dinheiro ou
        substituído por qualquer outra forma de compensação material.
        Consiste em inscrição para o congresso e passagem aérea em classe econômica
        ou transporte terrestre em veículo automotor (dependendo da
        localização de residência) e hospedagem em apto. single e restringe-se
        exclusivamente aos membros do grupo vencedor, não sendo extensivo
        a acompanhantes.
      </p>

      <p>
        Quaisquer despesas incorridas pelos participantes que receberem o apoio
        educacional acima descrito durante sua participação no Congresso serão
        de sua única e exclusiva responsabilidade.
      </p>

      <p>
        Haverá um material promocional para cada grupo vencedor (total de 3 materiais).
        Este material será escrito por todos os membros do grupo vencedor mais
        coordenador, e conterá: caso clínico vencedor e comentários sobre temas
        associados ao caso. Cada integrante do grupo receberá pagamento de
        honorários de R$ 3.500 (valor bruto, sendo que serão deduzidos impostos)
        para sua participação no material promocional. Caso algum integrante
        não queira participar como autor do material, não receberá o valor
        dos honorários e não terá seu nome presente no mesmo.
      </p>

      <h3>IX – DO CRONOGRAMA*</h3>

      <ul>
        <li>
          25 de fevereiro: prazo para indicações dos médicos coordenadores pela
          área comercial Novartis
        </li>
        <li>
          Março: prazo para validação e convite aos médicos coordenadores pela
          área médica Novartis
        </li>
        <li>
          18 de março: reunião presencial com os coordenadores e membros da
          comissão julgadora para apresentação do programa
        </li>
        <li>
          De 27 de março a 07 de agosto: reuniões dos grupos de trabalho para
          discussão dos casos clínicos.
        </li>
        <li>
          Até 07 de agosto: submissão dos casos clínicos para avaliação do comitê
           avaliador
        </li>
        <li>
          De 08 de agosto a 15 de setembro: avaliação dos relatos de casos clínicos
           pelo comitê avaliador
        </li>
        <li>
          30 de setembro: Fórum de encerramento com divulgação dos vencedores e
          cerimônia de premiação.
        </li>
        <li>
          2018: Premiação - publicação de material promocional com caso clínico
          comentado para cada grupo vencedor e apoio para Congresso Brasileiro de
          Dermatologia 2018 para todos os integrantes dos grupos vencedores.
        </li>
      </ul>

      <p>
        *Datas poderão sofrer alteração mediante comunicação aos participantes
      </p>

      <h3>X – CONSIDERAÇÕES FINAIS</h3>

      <p>
        Ao inscreverem-se para o Concurso CLEAR de Relatos de Casos Clínicos 2017,
        os participantes tacitamente concordam com as regras constantes deste
        documento, bem como a utilização sem ônus e sem qualquer contrapartida pela
        Novartis, de seu respectivo nome, imagem e trabalho para divulgação em
        qualquer meio de comunicação e publicações científicas, em âmbito
        nacional ou internacional, conforme legislação vigente, assim como
        publicação em materiais científicos da Novartis, que se compromete a identificar
        a autoria do material divulgado.
      </p>

      <p>
        As opiniões ou declarações contidas nos relatos de casos clínicos são de
        responsabilidade única e exclusiva dos autores dos relatos e não refletem
        necessariamente a posição da Novartis.
      </p>

      <p>
        Suspeitas de conduta antiética entre os participantes do Concurso e na
        elaboração dos casos clínicos serão apreciadas pelo Comitê Avaliador e,
        se consideradas procedentes, acarretarão sua desclassificação.
      </p>

      <p>
        Os relatos de caso clínico que não estejam de acordo com este regulamento
        também poderão ser desclassificados do Concurso CLEAR.
      </p>

      <p>
        Não se estabelece, pela força dessa iniciativa, de caráter essencialmente
        científico e educacional, qualquer vínculo de qualquer natureza entre a
        Novartis e os participantes.
      </p>

      <p>
        A Novartis não será responsável por nenhuma despesa incorrida pelo participante
        deste Concurso para a elaboração e envio dos seus trabalhos ou por qualquer ato
        do participante que possa causar danos materiais ou morais a sua pessoa ou de
        terceiros.
      </p>

      <p>
        O Participante responderá integralmente pela veracidade e acuidade dos dados
        cadastrados e enviados, incluindo os dados constantes do relato de caso inscrito,
        incluindo as respectivas referências.
      </p>

      <p>
        A Novartis se reserva no direito de alterar este regulamento a qualquer tempo,
        mediante notificação prévia a todos os participantes.
      </p>

      <p>
        Quaisquer questões relativas a esta iniciativa de caráter científico e educacional
        que não estejam endereçadas no presente Regulamento, serão avaliadas pelo Comitê
        Avaliador, cuja decisão será soberana, não sendo admitido questionamento posterior.
      </p>

      <p>
        Regulamento Versão 1.0<br>
        São Paulo, 31 de março de 2017
      </p>

      <p>
        Realização: NOVARTIS BIOCIÊNCIAS S/A
      </p>

    </div>
  </div>

@endsection
