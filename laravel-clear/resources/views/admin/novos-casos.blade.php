@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo" id="app-vue-admin">
    <div class="centralizar">

      <h2><span>NOVOS CASOS CLÍNICOS</span></h2>

      @if(session('sucesso'))
        <p class="alerta alerta-sucesso auto-close">
          {{session('sucesso')}}
        </p>
      @endif

      @if($errors->any())
        <p class="alerta alerta-erro">
          {{$errors->first()}}
        </p>
      @endif

      <p>
        <span>CATEGORIA 1</span> Caso clínico de paciente bio-naive (sem exposição prévia à terapia imunobiológica) em uso de secuquinumabe
      </p>

      <div class="linha-loader" v-if="!isLoaded"></div>
      <div class="lista-casos" v-cloak>
        @forelse($novosCat1 as $caso)
          <div class="linha">
            <div class="titulo">
              <a href="download-caso/{{$caso->codigo}}" title="Fazer download do arquivo">
                <span>{{$caso->codigo}}</span>
              </a>
            </div>
            <div class="data">
              <span>
                {{$caso->enviado_em->format('d/m/Y - H:i')}}
              </span>
            </div>
            <div class="distribuir">
              <a href="distribuir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('distribuir', '{{$caso->id}}', '{{$caso->codigo}}')">
                DISTRIBUIR PARA AVALIADORES
              </a>
            </div>
            <div class="remover">
              <a href="excluir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('excluir', '{{$caso->id}}', '{{$caso->codigo}}')">
                EXCLUIR
              </a>
            </div>
          </div>
        @empty
          <div class="linha">
            <div class="nenhum">Nenhum caso a ser distribuido nesta categoria</div>
          </div>
        @endforelse
      </div>

      <p>
        <span>CATEGORIA 2</span> Caso clínico de paciente com exposição prévia à terapia imunobiológica em uso de secuquinumabe
      </p>

      <div class="linha-loader" v-if="!isLoaded"></div>
      <div class="lista-casos" v-cloak>
        @forelse($novosCat2 as $caso)
          <div class="linha">
            <div class="titulo">
              <a href="download-caso/{{$caso->codigo}}" title="Fazer download do arquivo">
                <span>{{$caso->codigo}}</span>
              </a>
            </div>
            <div class="data">
              <span>
                {{$caso->enviado_em->format('d/m/Y - H:i')}}
              </span>
            </div>
            <div class="distribuir">
              <a href="distribuir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('distribuir', '{{$caso->id}}', '{{$caso->codigo}}')">
                DISTRIBUIR PARA AVALIADORES
              </a>
            </div>
            <div class="remover">
              <a href="excluir-caso/{{$caso->codigo}}" :disabled="!isLoaded" @click.prevent="mostrarAlerta('excluir', '{{$caso->id}}', '{{$caso->codigo}}')">
                EXCLUIR
              </a>
            </div>
          </div>
        @empty
          <div class="linha">
            <div class="nenhum">Nenhum caso a ser distribuido nesta categoria</div>
          </div>
        @endforelse
      </div>

    </div>

    <transition name="fade">
      <div class="modal" v-show="alerta.mostrar" v-cloak>
        <div class="backdrop" @click.stop="fecharModal"></div>
        <div class="modal-conteudo">
          <div class="confirm-box">
            <div v-if="alerta.tipo == 'distribuir'">
              <p>
                Confirmar a distribuição do arquivo do caso clínico para os Avaliadores.
              </p>
            </div>
            <div v-if="alerta.tipo == 'excluir'">
              <p>
                Confirmar a exclusão do arquivo do caso clínico.
              </p>
            </div>
            <p class="code"><strong>@{{alerta.codigo}}</strong></p>
            <div class="controles">
              <a href="#" @click.prevent="fecharModal" class="botao-voltar">CANCELAR</a>
              <a :disabled="disableAction" @click.prevent.once="window.location = alerta.destino" class="botao-envio" v-if="alerta.tipo == 'distribuir'">CONFIRMAR</a>
              <a :disabled="disableAction" @click.prevent.once="window.location = alerta.destino" class="botao-remover" v-if="alerta.tipo == 'excluir'">EXCLUIR</a>
            </div>
          </div>
        </div>
      </div>
    </transition>
  </div>

@endsection
