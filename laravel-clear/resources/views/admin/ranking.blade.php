@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo">
    <div class="centralizar">

      <h2><span>RANKING DE CASOS CLÍNICOS</span></h2>

      @if($publicarRanking)

        <div class="lista-ranking" data-label="CATEGORIA 1">
          @forelse($rankingCat1 as $caso)

            <div class="linha">
              <div class="ranking-item ranking-contador"><span></span></div>
              <div class="ranking-item ranking-codigo">
                <span>
                  {{$caso->codigo}}
                </span>
              </div>
              <div class="ranking-item ranking-autor" data-label="NOME DO AUTOR">
                <span>
                  {{$caso->autor}}
                </span>
              </div>
              <div class="ranking-item ranking-coautores" data-label="CO-AUTORES">
                @if(empty($caso->coautor_1) && empty($caso->coautor_2) && empty($caso->coautor_3) && empty($caso->coautor_4) && empty($caso->coautor_5))
                  <p class="vazio">
                    --
                  </span>
                @else
                  <p>
                    @if($caso->coautor_1)
                      {{$caso->coautor_1}}<br>
                    @endif
                    @if($caso->coautor_2)
                      {{$caso->coautor_2}}<br>
                    @endif
                    @if($caso->coautor_3)
                      {{$caso->coautor_3}}<br>
                    @endif
                    @if($caso->coautor_4)
                      {{$caso->coautor_4}}<br>
                    @endif
                    @if($caso->coautor_5)
                      {{$caso->coautor_5}}<br>
                    @endif
                  </p>
                @endif
              </div>
              <div class="ranking-item ranking-coordenador" data-label="NOME DO COORDENADOR">
                <span>
                  {{$caso->coordenador}}
                </span>
              </div>
              <div class="ranking-item ranking-pontos" data-label="TOTAL DE PONTOS">
                <span>
                  {{$caso->ranking}}
                </span>
              </div>
            </div>

          @empty

            <div class="linha">
              <div class="nenhum">
                Nenhum caso com pontuação
              </div>
            </div>

          @endforelse
        </div>

        <div class="lista-ranking" data-label="CATEGORIA 2">
          @forelse($rankingCat2 as $caso)

            <div class="linha">
              <div class="ranking-item ranking-contador"><span></span></div>
              <div class="ranking-item ranking-codigo">
                <span>
                  {{$caso->codigo}}
                </span>
              </div>
              <div class="ranking-item ranking-autor" data-label="NOME DO AUTOR">
                <span>
                  {{$caso->autor}}
                </span>
              </div>
              <div class="ranking-item ranking-coautores" data-label="CO-AUTORES">
                @if(empty($caso->coautor_1) && empty($caso->coautor_2) && empty($caso->coautor_3) && empty($caso->coautor_4) && empty($caso->coautor_5))
                  <p class="vazio">
                    --
                  </span>
                @else
                  <p>
                    @if($caso->coautor_1)
                      {{$caso->coautor_1}}<br>
                    @endif
                    @if($caso->coautor_2)
                      {{$caso->coautor_2}}<br>
                    @endif
                    @if($caso->coautor_3)
                      {{$caso->coautor_3}}<br>
                    @endif
                    @if($caso->coautor_4)
                      {{$caso->coautor_4}}<br>
                    @endif
                    @if($caso->coautor_5)
                      {{$caso->coautor_5}}<br>
                    @endif
                  </p>
                @endif
              </div>
              <div class="ranking-item ranking-coordenador" data-label="NOME DO COORDENADOR">
                <span>
                  {{$caso->coordenador}}
                </span>
              </div>
              <div class="ranking-item ranking-pontos" data-label="TOTAL DE PONTOS">
                <span>
                  {{$caso->ranking}}
                </span>
              </div>
            </div>

          @empty

            <div class="linha">
              <div class="nenhum">
                Nenhum caso com pontuação
              </div>
            </div>

          @endforelse
        </div>

      @else

        <p class="destaque">
          O Ranking estará disponível em <strong>{{env('SITE_PUBLICACAO_RANKING')}}</strong>.
        </p>

      @endif

    </div>
  </div>

@endsection
