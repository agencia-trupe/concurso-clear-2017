@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo">
    <div class="centralizar">

      <h2><span>STATUS DE ENVIO &middot; COORDENADORES</span></h2>

      <div class="lista-coordenadores">
      @foreach($coordenadores as $coord)
        <div class="linha">
          <div class="coordenador-nome">
            <span>
              {{$coord->nome}}
            </span>
          </div>
          <div class="coordenador-envio" data-label="CATEGORIA 1">
            <span>
              {!! $coord->StatusEnvioCaso1 !!}
            </span>
          </div>
          <div class="coordenador-envio" data-label="CATEGORIA 2">
            <span>
              {!! $coord->StatusEnvioCaso2 !!}
            </span>
          </div>
        </div>
      @endforeach
      </div>

    </div>
  </div>

@endsection
