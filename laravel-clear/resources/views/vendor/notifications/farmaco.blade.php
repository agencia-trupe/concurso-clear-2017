@component('mail::message')
# Novo caso clínico

## {{$titulo}}

{{$coordenador}}

Um novo arquivo foi enviado para revisão do administrador do Concurso Clear de Casos Clínicos. Segue anexo cópia do arquivo.

Atenciosamente,<br>
Comissão do Concurso CLEAR
@endcomponent
