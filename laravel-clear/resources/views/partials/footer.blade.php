<footer>
  <div class="centralizar">

    <div class="marca-academia">
      <img src="images/logo1.png">
    </div>

    <div class="marca-novartis">
      <img src="images/novartis.png">
    </div>

    <p>
      SIC | Serviço de Informações ao Cliente | <a href="mailto:sic.novartis@novartis.com" title="Entrar em contato">sic.novartis@novartis.com</a>
      <br>
      O uso deste site é governado por nossos <a href="termos-de-uso" title="Termos de Uso">Termos de Uso</a> e nossa <a href="politica-de-privacidade" title="Política de Privacidade">Política de Privacidade</a>.
      <br>
      &copy; {{date('Y')}} Novartis Biociências S.A.
    </p>

  </div>
</footer>
