<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('coordenador_id')->unsigned();
            $table->foreign('coordenador_id')->references('id')->on('usuarios')->onDelete('cascade');

            $table->integer('categoria'); // 1 ou 2
            $table->string('codigo')->nullable(); // gerado pela aplicação
            $table->string('arquivo')->nullable();
            $table->string('autor')->nullable();
            $table->string('coautor_1')->nullable();
            $table->string('coautor_2')->nullable();
            $table->string('coautor_3')->nullable();
            $table->string('coautor_4')->nullable();
            $table->string('coautor_5')->nullable();
            $table->datetime('enviado_em')->nullable();
            $table->datetime('distribuido_em')->nullable();
            $table->datetime('excluido_em')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casos');
    }
}
