<?php

use Keboola\Csv\CsvFile;
use Illuminate\Database\Seeder;

class UsuariosCsvTableSeeder extends Seeder
{
  public function run()
	{
    $table = 'usuarios';
    $table_casos = 'casos';

    $senha_padrao = 'senhateste';
    $arquivos = [
      'desenvolvimento' => 'usuarios_dev.csv',
      'homologacao' => 'usuarios_testes.csv',
      'producao' => 'usuarios_producao.csv'
    ];

    $filename = base_path().'/database/seeds/listas/'.$arquivos[env('LISTA_DE_USUARIOS')];

		// Recommended when importing larger CSVs
		DB::disableQueryLog();

		// Uncomment the below to wipe the table clean before populating
		DB::table($table)->delete();
    DB::table($table_casos)->delete();

    $csv = new CsvFile($filename);
    $senha_hash = Hash::make($senha_padrao);

    foreach($csv AS $k => $row) {
      if($k > 0 && $row[0] != '#'){

        $coordenador_id = DB::table($table)->insertGetId(
          [
            'tipo' => trim($row[0]),
            'nome' => trim($row[1]),
            'crm' => trim($row[2]),
            'cidade' => trim($row[3]),
            'estado' => trim($row[4]),
            'telefone' => trim($row[5]),
            'centro' => trim($row[6]),
            'grupo' => trim($row[7]),
            'email' => trim($row[8]),
            'senha_criada_em' => isset($row[9]) && $row[9] ? date('Y-m-d H:i:s') : null,
            'password' => $senha_hash
          ]
        );

        if($row[0] == 'coordenador'){
          // inserir 2 casos, 1 de cada categoria
          for ($c=1; $c <= 2; $c++) {
            DB::table($table_casos)->insert([
                'coordenador_id' => $coordenador_id,
                'categoria' => $c
            ]);
          }
        }

      }
    }

	}
}
