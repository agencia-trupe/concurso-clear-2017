<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      /*
      | Carregar lista de usuários definida no .env('LISTA_DE_USUARIOS')
      | Zera lista de Usuário e Casos. Cria 2 casos novos para cada coordenador
      */
       // $this->call(UsuariosCsvTableSeeder::class);

      /*
      | Reset de Casos na base de dados. Remove todos os casos e avaliações
      | e mantém os usuários como estão.
      */
      // $this->call(CasosTableSeeder::class);

    }
}
