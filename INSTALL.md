# INSTALAÇÃO
É importante que o diretório `/laravel-clear` fique fora do acesso público.

O domínio concursoclear.com.br deve apontar diretamente para o diretório `/public`.

## Permissões
É necessário conceder permissão de escrita `0777` nos diretórios:

`/laravel-clear/storage/logs`

`/laravel-clear/resources/assets/arquivos`

## Base de dados
O dump da base de dados MySQL está disponível com os usuários para o ambiente
 de produção no arquivo `dump.sql`.


## Usuários
Os cadastros ativos são:

```
usuario: concurso.clear@novartis.com
senha: senhateste

usuario: leila@trupe.net
senha: senhateste
```

Além dos cadastros de médicos, coordenadores e avaliadores (todos inativos)
que estavam na listagem enviada.

Os e-mails de ativação podem ser enviados pela administração acessando
diretamente o endereço `concursoclear.com.br/usuarios` após o login.


## Configurações

As configurações são definidas no arquivo `/laravel-clear/.env`.

As configurações das linhas destacadas abaixo devem estar preenchidas para funcionamento do sistema:

### Banco de dados
```
...
9.  DB_HOST=
10. DB_DATABASE=
11. DB_USERNAME=
12. DB_PASSWORD=
...

```

### SMTP
```
...
15. MAIL_HOST=
16. MAIL_PORT=
17. MAIL_USERNAME=
18. MAIL_PASSWORD=
...

```

### Outros
```
...
24. SITE_ARQUIVO_FORM="Caminho do arquivo do Formulário para download"
25. SITE_ARQUIVOS_DIR="Diretório de armazenamento de arquivos enviados (necessita permissão de escrita)"
26. SITE_DATA_MAX_ENVIO="Data máxima para envio de casos - (dd/mm/yyyy)"
27. SITE_DATA_MAX_AVALIACAO="Data máxima para avaliação de casos - (dd/mm/yyyy)"
28. SITE_PUBLICACAO_RANKING="Data de publicação do ranking - (dd/mm/yyyy)"
...

```
