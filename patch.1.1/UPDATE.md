# Atualização 1.1

## Atualizar aquivos:

### Controllers

```
laravel-clear/app/Http/Controllers\AdminController.php
```

### Views

```
laravel-clear/resources/views/admin/envios.blade.php
laravel-clear/resources/views/admin/usuarios.blade.php
```

### Rotas

```
laravel-clear/routes/web.php
```


### Estilos
```
public/css/app.css
```


## Nova base

A fim de evitar a exceção Swift_RfcComplianceException ao enviar e-mails:

O arquivo `dump.2.sql` contém uma cópia da base enviada anteriormente com uma
correção aplicada aos endereços fornecidos que não estavam de acordo com a
RFC 2822, 3.6.2.

Sobreescrever a base anterior com este arquivo antes de fazer o envio inicial.
