-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: clear
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `avaliacoes`
--

DROP TABLE IF EXISTS `avaliacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avaliacoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `casos_id` int(10) unsigned NOT NULL,
  `avaliador_id` int(10) unsigned NOT NULL,
  `criterio_1` decimal(3,1) DEFAULT NULL,
  `criterio_2` decimal(3,1) DEFAULT NULL,
  `criterio_3` decimal(3,1) DEFAULT NULL,
  `criterio_4` decimal(3,1) DEFAULT NULL,
  `criterio_5` decimal(3,1) DEFAULT NULL,
  `media` decimal(3,1) DEFAULT NULL,
  `avaliado_em` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `avaliacoes_casos_id_foreign` (`casos_id`),
  KEY `avaliacoes_avaliador_id_foreign` (`avaliador_id`),
  CONSTRAINT `avaliacoes_avaliador_id_foreign` FOREIGN KEY (`avaliador_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE,
  CONSTRAINT `avaliacoes_casos_id_foreign` FOREIGN KEY (`casos_id`) REFERENCES `casos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avaliacoes`
--

LOCK TABLES `avaliacoes` WRITE;
/*!40000 ALTER TABLE `avaliacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `avaliacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `casos`
--

DROP TABLE IF EXISTS `casos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `casos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coordenador_id` int(10) unsigned NOT NULL,
  `categoria` int(11) NOT NULL,
  `codigo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arquivo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `autor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coautor_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coautor_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coautor_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coautor_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coautor_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enviado_em` datetime DEFAULT NULL,
  `distribuido_em` datetime DEFAULT NULL,
  `excluido_em` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `casos_coordenador_id_foreign` (`coordenador_id`),
  CONSTRAINT `casos_coordenador_id_foreign` FOREIGN KEY (`coordenador_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `casos`
--

LOCK TABLES `casos` WRITE;
/*!40000 ALTER TABLE `casos` DISABLE KEYS */;
INSERT INTO `casos` VALUES (1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,3,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,9,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,9,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,15,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,15,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,20,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,20,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,26,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,26,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,32,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,32,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,37,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,37,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,43,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,43,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,49,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,49,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,55,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,55,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,61,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,61,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,67,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(24,67,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,73,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,73,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,79,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,79,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(29,85,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,85,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,91,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,91,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,97,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(34,97,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(35,103,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(36,103,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(37,117,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(38,117,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(39,123,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(40,123,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(41,129,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(42,129,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(43,135,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(44,135,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `casos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_erros`
--

DROP TABLE IF EXISTS `log_erros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_erros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hash` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `msg` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_erros`
--

LOCK TABLES `log_erros` WRITE;
/*!40000 ALTER TABLE `log_erros` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_erros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=415 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (410,'2014_10_12_000000_create_users_table',1),(411,'2014_10_12_100000_create_password_resets_table',1),(412,'2017_05_26_150221_create_casos_table',1),(413,'2017_05_26_150230_create_avaliacoes_table',1),(414,'2017_05_31_154557_create_log_erros_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recuperar_senha`
--

DROP TABLE IF EXISTS `recuperar_senha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recuperar_senha` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `recuperar_senha_email_index` (`email`),
  KEY `recuperar_senha_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recuperar_senha`
--

LOCK TABLES `recuperar_senha` WRITE;
/*!40000 ALTER TABLE `recuperar_senha` DISABLE KEYS */;
/*!40000 ALTER TABLE `recuperar_senha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `crm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cidade` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `centro` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grupo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_criacao_senha` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_enviado_em` datetime DEFAULT NULL,
  `senha_criada_em` datetime DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'admin','Administrador Teste','','São Paulo','SP','','','','concurso.clear@novartis.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,'2017-06-30 12:40:38',NULL,NULL,NULL),(2,'admin','Administrador Teste Trupe','','São Paulo','SP','','','','leila@trupe.net','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,'2017-06-30 12:40:38',NULL,NULL,NULL),(3,'coordenador','Daniel Lorenzini','24838/RS','Porto Alegre','RS','51 32394856/ 982038796','CT Ana Moreira','A','daniellorenzini@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(4,'medico','Nicolle Mazzotti','30010/RS','Porto Alegre','RS','51 32220111/ 992838573','CT Ana Moreira','A','nicollemazzotti@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(5,'medico','Juliana Catucci Boza','31807/RS','Porto Alegre','RS','51 32645684/ 996521887','CT Ana Moreira','A','juliana_boza@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(6,'medico','Leandro Leite','39057/RS','Porto Alegre','RS','51 980491809','CT Ana Moreira','A','leitelll@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(7,'medico','Leonardo Cabeda','24015/RS','Porto Alegre','RS','51 32324540/ 99550537','CT Ana Moreira','A','lfcabeda@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(8,'medico','Samanta Daiana De Rossi','38556/RS','Porto Alegre','RS','51 981629318','CT Ana Moreira','A','samantaderossi@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(9,'coordenador','Caio César Silva de Castro','CRMPR:12957','Curitiba','PR','041-99685-5023','CT Alessandro Dutra','A','Caio.castro@pucpr.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(10,'medico','Adriane Reichert Faria','CRMPR:20445','Curitiba','PR','041-98806-3870','CT Alessandro Dutra','A','adri_reichert@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(11,'medico','Anelise Rocha Raumundo','CRMPR:28316','Curitiba','PR','041-99644-5904','CT Alessandro Dutra','A','aneliserr@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(12,'medico','José Roberto Toshio Shibue','CRMPR:13613','Curitiba','PR','041-99912-0999','CT Alessandro Dutra','A','shibue@uol.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(13,'medico','Felipe Bochina Cerci','CRMPR:24825','Curitiba','PR','041-99247-1009','CT Alessandro Dutra','A','cercihc@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(14,'medico','Gerson Dellatorre','CRMPR:26980','Curitiba','PR','041-99995-4678','CT Alessandro Dutra','A','dellatorrederm@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(15,'coordenador','Anber Ancel Tanaka','CRMPR:17009','Curitiba','PR','041-99966-3618','CT Alessandro Dutra','B','anbertanaka@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(16,'medico','Rafael Garani','CRMPR:20900','Londrina','PR','043-99613-4800','CT Alessandro Dutra','B','rafaelgarani@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(17,'medico','Lígia Mario Martin','CRMPR:9763','Londrina','PR','043-99993-9528','CT Alessandro Dutra','B','ligiadermato@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(18,'medico','Brunno Zeni de Lima','CRMPR:26604','Sao Jose Dos Pinhais','PR','041-99971-1807','CT Alessandro Dutra','B','dr.brunno.dermatologista@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(19,'medico','Dalton Kojima','CRMPR:18603','Curitiba','PR','041-98418-3911','CT Alessandro Dutra','B','daltonkojima@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(20,'coordenador','Daniel Hotlhausen Nunes','CRMSC:7820','Florianopolis','SC','048-98404-4473','CT Alessandro Dutra','C','daniel@floripa.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(21,'medico','Maurício Amboni Conti','CRMSC:9002','Itajai','SC','047-99988-5505','CT Alessandro Dutra','C','mauricio@conti.med.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(22,'medico','Roberto Amorim Filho','CRMSC:5312','Florianopolis','SC','048-99972-0876','CT Alessandro Dutra','C','amorimf.roberto@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(23,'medico','Breno Marzola da Silveira','17660/RS','Bento Gonçalves','SC','(54) 999722686/ 34524723','CT Alessandro Dutra','C','bmarzola@ig.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(24,'medico','Mariane Corrêa Fissmer','CRMSC:12134','Tubarao','SC','048-99109-1400','CT Alessandro Dutra','C','marianecfissmer@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(25,'medico','Miriam Popoaski','CRMSC:12201','Tubarao','SC','048-99613-6868','CT Alessandro Dutra','C','miriampopoaski@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(26,'coordenador','Eduardo Lacaz Martins','69294','São Paulo','SP','11 99408-5841','CT Adriana Moraes','A','edulacaz@terra.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(27,'medico','Ana Paula Galli Sanchez','81979','São Paulo','SP','11 96175-3232','CT Adriana Moraes','A','apc-med@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(28,'medico','Cristina Martinez Zugaib Abdalla','55807','São Paulo','SP','11 99979-0776','CT Adriana Moraes','A','dra.abdalla@terra.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(29,'medico','Vera Aun Palma','141837','Santo André','SP','11 97120-4662','CT Adriana Moraes','A','vera_aun@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(30,'medico','Alessandra Vanessa Pellegrino','129588','São Paulo','SP','11 99484-6102','CT Adriana Moraes','A','clinicaessencele@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(31,'medico','Alan Ost','152844','São Paulo','SP','11 95027-2527','CT Adriana Moraes','A','alan01ost@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(32,'coordenador','Maria Cecilia Carvalho Bortoletto','61063','Santo André','SP','11 99404-0862','CT Adriana Moraes','B','ceciliabortoletto@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(33,'medico','Wagner Guimarães Galvão Cesar','116138','São Paulo','SP','11 96186-3279','CT Adriana Moraes','B','wagnergalvao@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(34,'medico','Andreia Castanheiro da Costa','101371','Santo André','SP','11 99255-5031','CT Adriana Moraes','B','dra_accosta@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(35,'medico','Bruna Elena Graciano Falcone','121886','Santo André','SP','11 97473-6260','CT Adriana Moraes','B','falconebruna@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(36,'medico','Mariana Fátima Muaccad Gama','132272','Santo André','SP','11 98752-5896','CT Adriana Moraes','B','mfmgama@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(37,'coordenador','Marcelo Arnone','90826','São Paulo','SP','11 99907-2787','CT Erika Tamashiro','A','arnones@uol.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(38,'medico','André Hirayama','120220','São Paulo','SP','11 99972-4729','CT Erika Tamashiro','A','xkrandre1979@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(39,'medico','Beni Grinblat','84426','São Paulo','SP','11 981118030','CT Erika Tamashiro','A','derma@grinblat','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(40,'medico','Luciana Maragno','116076','São Paulo','SP','11 995710124','CT Erika Tamashiro','A','lucianamaragno@clinicaliv.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(41,'medico','Aline Okida','','Mogi das Cruzes','SP','','CT Erika Tamashiro','A','alinelissaokita@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(42,'medico','Hebert Roberto C. Brandt','114237','Ourinhos','SP','14 99894-2222','CT Erika Tamashiro','A','hebertbrandt@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(43,'coordenador','Artur Duarte','43224','São Paulo','SP','11 99603-9854','CT Erika Tamashiro','B','drartur@drartur.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(44,'medico','Carla Bortoloto','122883','São Paulo','SP','11 99494-6992','CT Erika Tamashiro','B','cbortoloto@ig.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(45,'medico','Seomara Passos Catalano','69246','São Paulo','SP','11 97339-9252','CT Erika Tamashiro','B','seocatalano@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(46,'medico','Juliana Nakano','107806','São Paulo','SP','11 98755-6360','CT Erika Tamashiro','B','julinakano@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(47,'medico','Clarice Kobata','85480','São Paulo','SP','11 99198-1545','CT Erika Tamashiro','B','claricekobata@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(48,'medico','Solange Marcondes','56706','Osasco','SP','11 99980-5479','CT Erika Tamashiro','B','jlgomes01@uol.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(49,'coordenador','Maria Vitória Suarez','181065','São Paulo','SP','11  97491-2009','CT Fabricio Rosa','A','mariavsuarez@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(50,'medico','Juliana Fernandes','127407','Guarulhos','SP','11 99277-4014','CT Fabricio Rosa','A','jucfernandes@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(51,'medico','Adriana Porro','54292','São Paulo','SP','11 976321506','CT Fabricio Rosa','A','adriana.porro@uol.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(52,'medico','Eugenia Maria Ohe','11516','São Paulo','SP','11 983696767','CT Fabricio Rosa','A','eugeniadamasio@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(53,'medico','Domingos','72606','São Paulo','SP','11 99963-5206','CT Fabricio Rosa','A','domingos_simone@uol.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(54,'medico','Maria Cristina','33720','São Paulo','SP','11 99771-0177','CT Fabricio Rosa','A','mmcrisj@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(55,'coordenador','Luis Antônio de Paula Machado','56612','São Paulo','SP','11 98255-3284','CT Fabricio Rosa','B','luismachadoderma@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(56,'medico','Fernanda Caldeira','101062','São José Dos Campos','SP','11 97177-0370','CT Fabricio Rosa','B','pintocaldeira@uol.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(57,'medico','David Gruman','79101','Osasco','SP','11 99963-5444','CT Fabricio Rosa','B','gruman@globo.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(58,'medico','Sarah  Freua','42653','São Paulo','SP','11 99900-6166','CT Fabricio Rosa','B','drasarahfreua@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(59,'medico','Raimundo Saraiva','79069','São Paulo','SP','11 98786-8394','CT Fabricio Rosa','B','amanda.providence@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(60,'medico','Fábio Sakamoto','139290','São Paulo','SP','11 96384-8052','CT Fabricio Rosa','B','fabiosakamoto@doctor.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(61,'coordenador','Cacilda da Silva Souza','52027','Ribeirão Preto','SP','016 99788-0255','CT Anderson Capelari','A','cssouza@fmrp.usp.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(62,'medico','Renato Soriani Paschoal','121106','Ribeirão Preto','SP','016 991766932/32362648','CT Anderson Capelari','A','renatosoriani@rspdermato.med.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(63,'medico','Daniel Elias','134113','Ribeirão Preto','SP','016991080814','CT Anderson Capelari','A','danieleliasfmrp@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(64,'medico','Weber Soares Coelho','123385','Sertãozinho','SP','01698122-2926','CT Anderson Capelari','A','wscoelho15@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(65,'medico','Renata Nahas Cardilli','81382','Ribeirão Preto','SP','(016)997966746/1632355811','CT Anderson Capelari','A','nahas-renata@uol.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(66,'medico','Roberto  Bueno Filho','125688','Ribeirão Preto','SP','(016) 98119-3893','CT Anderson Capelari','A','rbueno@rivve.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(67,'coordenador','Joao Roberto ANTONIO','11784','São José Do Rio Preto','SP','01732326611/017981188849','CT Anderson Capelari','B','dr.joao@terra.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(68,'medico','Vera Lúcia Naser','24125','São José Do Rio Preto','SP','1732351471','CT Anderson Capelari','B','veralanaser@gmail.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(69,'medico','Eurides Pozetti','27886','São José Do Rio Preto','SP','017997830672/01732242155','CT Anderson Capelari','B','gpozetti@terra.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(70,'medico','Tania Regina BARBON','38862','São José Do Rio Preto','SP','(17)3232-0784/(17)991290015','CT Anderson Capelari','B','clinica.alvorada@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(71,'medico','Guilherme Bueno de Oliveira','135574','São José Do Rio Preto','SP','(17)99622-6529','CT Anderson Capelari','B','mggbueno@uol.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(72,'medico','Silvia Roberta Tirelli Rocha','66635','São José Do Rio Preto','SP','(17)99772-1531/(17)32327247','CT Anderson Capelari','B','dermat.silviatirelli@terra.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(73,'coordenador','Renata Magalhães','88163','Campinas','SP','19-99199-1670','CT Mauricio Telles','A','Renatafmagalhaes_dra@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(74,'medico','Daniela Bellucci','89858','Sousas','SP','19-99898-1464','CT Mauricio Telles','A','danbellucci@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(75,'medico','Juliana Yumi','134967','Campinas','SP','19-9615-5502','CT Mauricio Telles','A','Juliana.massuda@esmerasaude.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(76,'medico','Luciena Cegatto Martins Ortigosa','88852','Presidente Prudente','SP','18-98131-0353','CT Mauricio Telles','A','Luciena.ortigosa@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(77,'medico','Luciane Miot','90420','Botucatu','SP','14-99775-8021','CT Mauricio Telles','A','Lucianemiot@fmb.unesp.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(78,'medico','Valquiria Pessoa Chinen','104254','Botucatu','SP','14-99184-6078','CT Mauricio Telles','A','Valpessoa@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(79,'coordenador','Ana Carolina Bazan Arruda','124577','Jundiai','SP','11-99517-1653','CT Mauricio Telles','B','Acbbazan@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(80,'medico','Paula Colpas','129556','Campinas','SP','19-98138-8221','CT Mauricio Telles','B','paulacolpas@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(81,'medico','André Simião','124893','Jundiai','SP','11-99636-6571','CT Mauricio Telles','B','Andresimiao@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(82,'medico','Ana Helena Kalies Oliveira','157974','Campinas','SP','19-99884-2027','CT Mauricio Telles','B','Ahkalies@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(83,'medico','Elisa Trino de  Moraes','146267','Campinas','SP','19-98128-0636','CT Mauricio Telles','B','Elisamoraes36@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(84,'medico','Célia Xavier','50272','Jundiai','SP','11-99946-4265','CT Mauricio Telles','B','Celia.dermato@terra.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(85,'coordenador','Juliana Martins Leal','','Rio de Janeiro','RJ','','CT Ismael Junior','1','Julianamartinsleal@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(86,'medico','Alexandre Gripp','','Rio de Janeiro','RJ','','CT Ismael Junior','1','alexandregripp@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(87,'medico','Bárbara Nader','','Rio de Janeiro','RJ','','CT Ismael Junior','1','bnvasconcelos@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(88,'medico','Aline Bressan','','Rio de Janeiro','RJ','','CT Ismael Junior','1','alibressan@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(89,'medico','Ana Luísa Sampaio','','Rio de Janeiro','RJ','','CT Ismael Junior','1','analuisasbs@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(90,'medico','Gabriela Higino','','Rio de Janeiro','RJ','','CT Ismael Junior','1','gabi.h.s@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(91,'coordenador','Paulo Oldani Félix','','Rio de Janeiro','RJ','','CT Ismael Junior','2','paulooldani@globo.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(92,'medico','João Avelleira','','Rio de Janeiro','RJ','','CT Ismael Junior','2','Avelleira@uol.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(93,'medico','Roberto Souto','','Niterói','RJ','','CT Ismael Junior','2','rs_souto@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(94,'medico','Livia Barbosa','','Rio de Janeiro','RJ','','CT Ismael Junior','2','barbosalivia@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(95,'medico','Vêronica Bogado','','Rio de Janeiro','RJ','','CT Ismael Junior','2','vebogado@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(96,'medico','Claudia Maia','','Rio de Janeiro','RJ','','CT Ismael Junior','2','cpamaia@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(97,'coordenador','Gleison V. Duarte','','Salvador','BA','','CT Francisco Lima','1','gleisonvduarte@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(98,'medico','Rafael Barreto','','Salvador','BA','','CT Francisco Lima','1','rafabpcarvalho@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(99,'medico','Mariá Souza','','Salvador','BA','','CT Francisco Lima','1','ecto@uol.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(100,'medico','Ivonise Follador','','Salvador','BA','','CT Francisco Lima','1','ifollador@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(101,'medico','Fabíola Leal','','Salvador','BA','','CT Francisco Lima','1','fabiolaleal12@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(102,'medico','Maria de Fatima Paim','','Salvador','BA','','CT Francisco Lima','1','mfatimapaim@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(103,'coordenador','Sueli Carneiro','','Rio de Janeiro','RJ','','CT Francisco Lima','2','sueli@hucff.ufrj.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(104,'medico','Rachel Grysphan','','Rio de Janeiro','RJ','','CT Francisco Lima','2','rachelgrp@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(105,'medico','Shirley Gamonal','','Rio de Janeiro','RJ','','CT Francisco Lima','2','shirleygamonal@terra.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(106,'medico','Flavia de Freire Cassia','','Juiz de Fora','RJ','','CT Francisco Lima','2','flaviafcassia@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(107,'medico','Cecília Victer','','Juiz de Fora','RJ','','CT Francisco Lima','2','cecilialag@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(108,'medico','Claudia Medeiros dos Santos Camargo','','Rio de Janeiro','RJ','','CT Francisco Lima','2','cda.claudiacamargo@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(109,'medico','Michelle Diniz','','Belo Horizonte','MG','','CT Luciana Lacerda','1','michellesdmi@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(110,'medico','Patrícia Avila','','Belo Horizonte','MG','','CT Luciana Lacerda','1','saudesanthe@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(111,'medico','Petra oliveira','','Belo Horizonte','MG','','CT Luciana Lacerda','1','petra.dermatologia@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(112,'medico','Mabelly Gouthier','','Belo Horizonte','MG','','CT Luciana Lacerda','1','mabelyadg@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(113,'medico','Rosane Costa','','Alfenas','MG','','CT Luciana Lacerda','2','costa.rosane@uol.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(114,'medico','Marina Costa','','Alfenas','MG','','CT Luciana Lacerda','2','marinaderm@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(115,'medico','Cilene Pelucio','','Três Corações','MG','','CT Luciana Lacerda','2','cilenepelucio@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(116,'medico','Flavia Medeiros','','Juiz de Fora','MG','','CT Luciana Lacerda','2','flaviareis.mg@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(117,'coordenador','Gladys Aires','','Brasília','DF','','CT Carlos Rabelo','1','gladys.amartins@uol.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(118,'medico','Leticia Oba','','Brasília','DF','','CT Carlos Rabelo','1','leticiaoba@yahoo.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(119,'medico','Mariana Costa','','Brasília','DF','','CT Carlos Rabelo','1','dermacosta@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(120,'medico','Claudio Lerer','','Brasília','DF','','CT Carlos Rabelo','1','claudiolerer@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(121,'medico','Patricia Shu Kurizky','','Brasília','DF','','CT Carlos Rabelo','1','patyshu79@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(122,'medico','Luanna Caires Portela','','Brasília','DF','','CT Carlos Rabelo','1','draluannaportela@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(123,'coordenador','Ana Maria Quintero','','Goiânia','GO','','CT Carlos Rabelo','2','anamqribeiro@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(124,'medico','Mirian Lane','','Goiânia','GO','','CT Carlos Rabelo','2','miriancastilho@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(125,'medico','Mayra Inhanes','','Goiânia','GO','','CT Carlos Rabelo','2','ianhez@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(126,'medico','Glaydson Gerônimo','','Goiânia','GO','','CT Carlos Rabelo','2','drglaydson@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(127,'medico','Sulamita','','Goiânia','GO','','CT Carlos Rabelo','2','sulamitacostac@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(128,'medico','Naiana','','Goiânia','GO','','CT Carlos Rabelo','2','nayaveiro@yahoo.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(129,'coordenador','Esther Barros Palitot','','João Pessoa','PB','','CT Edison Carvalho','1','estherpalitot@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(130,'medico','Mohammed Azzous','','João Pessoa','PB','','CT Edison Carvalho','1','azzouz@globo.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(131,'medico','Francisco das Chagas','','Salvador','BA','','CT Edison Carvalho','1','chagasnetodermato@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(132,'medico','Alessandra Braz','','João Pessoa','PB','','CT Edison Carvalho','1','alessandrabraz@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(133,'medico','Jonnia Sherlock','','Aracaju','SE','','CT Edison Carvalho','1','jonniasherlock@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(134,'medico','Maggy Poti','','Fortaleza','CE','','CT Edison Carvalho','1','maggypoti@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(135,'coordenador','Aldejane Gurgel','','Recife','PE','','CT Edison Carvalho','2','aldejane.gurgel@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(136,'medico','Alzirton de Lima Freire','','Recife','PE','','CT Edison Carvalho','2','alzirton@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(137,'medico','Regio Girão','','Teresina','PI','','CT Edison Carvalho','2','regiogirao@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(138,'medico','Paulo Roberto Guedes','','Recife','PE','','CT Edison Carvalho','2','prmg55@uol.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(139,'medico','Carolina Chacon','','Recife','PE','','CT Edison Carvalho','2','carolina_23@yahoo.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(140,'medico','Angela Valença','','Recife','PE','','CT Edison Carvalho','2','simoniderma@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(141,'avaliador','André Carvalho','24016','Porto Alegre','SC','','','','avecarvalho@me.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(142,'avaliador','Aripuanã Cobério','28455','Belo Horizonte','MG','','','','aripuana.coberio@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(143,'avaliador','Lincoln Fabricio','15009','Curitiba','PR','','','','lincolnzfabricio@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(144,'avaliador','Luiza Keiko','28645','São Paulo/Santo André','SP','','','','luizakeiko@uol.com.br','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(145,'avaliador','Luna Azulay','349634','Rio de Janeiro','RJ','','','','lunaazulay@gmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL),(146,'avaliador','Ricardo Romiti','75298','São Paulo','SP','','','','rromiti@hotmail.com','$2y$10$DElfK6w3UqiGEjSM6INP5uJTZbtuhv3HaC3W0C8KVnZ8Ac5YsjHmO',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-30 12:41:05
